<?php

use Illuminate\Support\Facades\Route;
use App\Informacao;
use App\Servicos;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/teste', function () {
    return view('paginas/testeauth');
});

Route::get('/','PaginaPrincipal@index');

Route::get('contato', 'Contatos@index')->name('contato');
Route::post('contato/cadastro', 'Contatos@store')->name('contatoCads');

Route::get('/sobre','Sobre@index');

Route::get('/servico/{id}','ServicosController@show');

Route::get('/servicos', 'ServicosController@index');

Auth::routes();
//use a rota http://127.0.0.1:8000/register para registrar
Route::get('/home', 'HomeController@index')->name('home');

route::group([
    'prefix'=> 'adm',
    'middleware' => 'auth'
], function(){
    Route::get('/contato', 'Contatos@list')->name('messagem');
    Route::get('/contato/mostrar/{msg}', 'Contatos@show')->name('messagemMostrar');
    Route::get('/contato/oculta/{msg}', 'Contatos@update')->name('messagemOcultar');
    Route::get('/contato/exibe','Contatos@exibeConteudo')->name('conteudoContato');
    route::post('/contato/alterar/{msg}','Contatos@edit')->name('conteudoAltContato');
    Route::get('/info','InformacaoController@index')->name('infoGes');
    Route::post('/info/alterar','InformacaoController@update')->name('infoUp');
    Route::get('/sobre','sobre@list')->name('gestSobre');
    Route::get('/sobre/oculta/{cont}','Sobre@oculta')->name('conteudoOcultar');
    Route::get('/sobre/editar/{cont}','Sobre@show')->name('conteudoMostrar');
    Route::post('/sobre/update/{cont}','Sobre@edit')->name('conteudoAlterar');

    //Rotas dos serviços
    Route::get('/servico_painel', 'ServicosController@indexPainel')->name('mostraServicos');
    Route::get('/servico_novo', 'ServicosController@create')->name('servico');
    Route::post('/insert_servico', 'ServicosController@store')->name('insertServico');
    Route::post('/alterar_servico/{id}', 'ServicosController@update')->name('alterarServico');
    Route::get('/servico_delet/{id}', 'ServicosController@destroy')->name('deletServico');
    Route::get('/foto_delet/{id}', 'ServicosController@deletFoto')->name('deletFoto');
    Route::get('/servico_edit/{id}', 'ServicosController@edit')->name('updateServico');
    Route::post('/servico_addFoto', 'ServicosController@addFoto')->name('addFoto');
    

});
