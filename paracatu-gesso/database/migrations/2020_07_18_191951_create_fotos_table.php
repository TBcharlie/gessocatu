<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotos', function (Blueprint $table) {
            $table->id();
            $table->string('titulo', 255);
            $table->string('descricao', 255);
            $table->boolean('visivel', 255);
            $table->string('foto', 255);
            $table->string('tipo', 255);
            $table->timestamps();
            $table->bigInteger('page_fk')->unsigned();
            $table->bigInteger('servico_fk')->unsigned();

            $table->foreign('page_fk')->references('id')->on('paginas');
            $table->foreign('servico_fk')->references('id')->on('servicos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fotos');
    }
}
