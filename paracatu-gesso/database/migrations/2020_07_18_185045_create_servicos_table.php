<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->id();
            $table->text('descricao',500);
            $table->string('nome',200);
            $table->string('foto_slideshow',200);
            $table->string('foto_principal',200);
            $table->timestamps();

            $table->bigInteger('page_fk')->unsigned();
            $table->foreign('page_fk')->references('id')->on('paginas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicos');
    }
}
