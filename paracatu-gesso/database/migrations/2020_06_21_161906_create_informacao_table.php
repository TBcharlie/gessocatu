<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informacao', function (Blueprint $table) {
            $table->id();
            $table->string('nome',100);
            $table->string('cnpj',20);
            $table->string('descricao',255);
            $table->string('telefone',20);
            $table->string('whatsapp',20);
            $table->string('instagram',150);
            $table->string('email',150);
            $table->string('logo',150);
            $table->string('posMapa',500);
            $table->string('logadouro',150);
            $table->string('bairro',150);
            $table->string('numLocal',5);//pois pode ser um imovel com o numero 5a ou só 5
            $table->string('cidade',50);
            $table->string('cep',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informacao');
    }
}
