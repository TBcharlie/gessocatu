<?php

use Illuminate\Database\Seeder;

class PaginasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginas')->insert([
            'titulo' => 'Sobre',
            'descricao' => 'Essa pagina contem algumas informações sobre a empresa, como missão, valores e objetivos'
        ]);
        DB::table('paginas')->insert([
            'titulo' => 'Serviços',
            'descricao' => 'Essa pagina contem algumas informações sobre os serviços prestado pela empresa'
        ]);
        DB::table('paginas')->insert([
            'titulo' => 'Contato',
            'descricao' => 'Essa pagina cotem alguma mensagem para o usuario que vai entrar em contato'
        ]);
    }
}
