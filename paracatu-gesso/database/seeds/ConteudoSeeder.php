<?php

use Illuminate\Database\Seeder;
use App\Paginas;
class ConteudoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paginas = Paginas::firstWhere('titulo', 'Sobre');
        DB::table('conteudo_pags')->insert([
            'titulo' => 'QUEM SOMOS NÓS',
            'conteudo' => '<p>Conteudo em manuteção</p>',
            'idPag' => $paginas->id,
            'visivel' => 1
        ]);
        DB::table('conteudo_pags')->insert([
            'titulo' => 'Nossa missão',
            'conteudo' => '<p>Conteudo em manuteção</p>',
            'idPag' => $paginas->id,
            'visivel' => 1
        ]);
        DB::table('conteudo_pags')->insert([
            'titulo' => 'Nossa visão',
            'conteudo' => '<p>Conteudo em manuteção</p>',
            'idPag' => $paginas->id,
            'visivel' => 1
        ]);
        DB::table('conteudo_pags')->insert([
            'titulo' => 'Nossos valores',
            'conteudo' => '<p>Conteudo em manuteção</p>',
            'idPag' => $paginas->id,
            'visivel' => 1
        ]);
        $paginas = Paginas::firstWhere('titulo', 'Contato');
        DB::table('conteudo_pags')->insert([
            'titulo' => 'BEM VINDO!',
            'conteudo' =>'<p>Deixe uma messagem de sugestão, duvida ou para orçamentos.</p>',
            'idPag' => $paginas->id,
            'visivel' => 1
        ]);
    }
}
