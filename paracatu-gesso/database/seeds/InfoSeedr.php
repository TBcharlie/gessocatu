<?php

use Illuminate\Database\Seeder;

class InfoSeedr extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('informacao')->insert([
            'nome' => 'Gesso Paracatu',
            'cnpj' => '10.693.807/0001-00',
            'descricao' => 'Conteudo em manutenção',
            'telefone' => '(38) 3672-4368',
            'whatsapp' => '(38) 9 9842-8403',
            'email' => 'paracatugesso@gmail.com',
            'logo' => '',
            'instagram' => 'https://www.instagram.com/paracatu_gesso/?hl=pt-br',
            'posMapa' =>    'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3810.581200485945!2d-46.885937185125854!3d-17.239094388167718!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94a84bbc9541ec8b%3A0x37bd3606bebbc3cc!2sParacatu%20Gesso!5e0!3m2!1spt-BR!2sbr!4v1597618548284!5m2!1spt-BR!2sbr',
            'logadouro' => 'Rua Aeroporto',
            'bairro' => 'Paracatuzinho',
            'numLocal' => '444',
            'cidade' => 'Paracatu - mg',
            'cep' => '38603-198'
        ]);
 
    }
}
