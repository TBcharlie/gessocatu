<?php

use App\ConteudoPag;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(InfoSeedr::class);
         $this->call(PaginasSeeder::class);
         $this->call(ConteudoSeeder::class);
    }
}
