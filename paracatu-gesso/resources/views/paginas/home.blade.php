@extends('templates/site')
@section('content')
<div id="mu-slider">
    <div class="mu-slide">
        <!-- Start single slide  -->
        @foreach($servicos as $servico)
        <div class="mu-single-slide">
            <img src="{{url($servico->foto_slideshow)}}" alt="slider img">
            <div class="mu-single-slide-content-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mu-single-slide-content">
                                <h1>{{$servico->nome}}</h1>
                                <p>{{$servico->descricao}}</p>
                                <a class="mu-primary-btn" href="{{url('/servicos')}}">Saiba Mais<span class="fa fa-long-arrow-right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <!-- End single slide  -->
    </div>
</div>
<main>
    <!-- Start About -->
    <section id="mu-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-about-area">
                        <!-- Title -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mu-title">
                                    <h2>{{ $sobre->titulo }}</h2>
                                    <?= $sobre->conteudo; ?>
                                    <a class="mu-primary-btn" href="/sobre">Saiba Mais<span class="fa fa-long-arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                        <!-- Start Feature Content -->
                        <div class="d-flex justify-content-center">
                            <div class="col-md-6">
                                <div class="mu-about-left">
                                    <img class="" src="assets/images/about-us.jpg" alt="img">
                                </div>
                            </div>
                        </div>
                        <!-- End Feature Content -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About -->

    <!-- Call to Action -->
    <div id="mu-call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-call-to-action-area">
                        <div class="mu-call-to-action-left">
                            <h2>Entre em contato agora mesmo e solicite um orçamento dos nossos serviços</h2>
                            <p>É de <b>GRAÇA!😎</b></p>
                        </div>
                        <a href="/contato" class="mu-primary-btn mu-quote-btn">Entrar em contato<i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Services -->
    <section id="mu-service">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-service-area">
                        <!-- Title -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mu-title">
                                    <h2>Nossos serviços</h2>
                                    <p>"breve descrição da qualidade dos serviços prestados pela empresa" consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa cum sociis.</p>
                                    <a class="mu-primary-btn" href="/servicos">Saiba Mais<span class="fa fa-long-arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                        <!-- Start Service Content -->
                     
                        <!-- End Service Content -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Services -->
</main>

@endsection