@extends('templates/painel')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Messagem de contato</h1>
    <a href="{{ route("messagem") }}" class="btn btn-danger btn-icon-split">
      <span class="icon text-white-50">
        <i class="fas fa-chevron-left"></i>
      </span>
      <span class="text">Voltar</span>
    </a>
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Assunto: {{ $mensagem->assunto}}</h6>
        <h6 class="m-0 font-weight-bold text-primary">Autor: {{ $mensagem->nome}}</h6>
      </div>
      <div class="card-body">
          <p class="card-text">{{ $mensagem->mensagem }}</p>
      </div>
    </div>

  </div>

@endsection