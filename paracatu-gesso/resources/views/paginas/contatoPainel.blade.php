@extends('templates/painel')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Messagem de contato</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tabela de messagem</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Ações</th>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Email</th>
                <th>Uf</th>
                <th>Cidade</th>
                <th>Assunto</th>
                <th>Data de envio</th>
              </tr>
            </thead>
            <tfoot>
                <th>Ações</th>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Email</th>
                <th>Uf</th>
                <th>Cidade</th>
                <th>Assunto</th>
                <th>Data de envio</th>
            </tfoot>
            <tbody>
              @foreach($lista as $msg)
              <tr>
                <td>
                    <a href="{{ route('messagemMostrar',$msg->id) }}"><i class="fas fa-envelope"></i></a>
                    <a href="{{ route('messagemOcultar',$msg->id) }}"><i class="fas fa-trash"></i></a>
                </td>
                <td>{{$msg->nome}}</td>
                <td>{{$msg->telefone}}</td>
                <td>{{$msg->email}}</td>
                <td>{{$msg->uf}}</td>
                <td>{{$msg->cidade}}</td>
                <td>{{$msg->assunto}}</td>
                <td>{{$msg->created_at}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>

@endsection