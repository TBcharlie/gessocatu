@extends('templates/painel')
@section('content')

<body>
  <div class="container">
    @if (session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
    @endif
    @if (session('error'))
    <div class="alert alert-danger">
      {{ session('error') }}
    </div>
    @endif
    <div class="row justify-content-md-center service_form">
      <h1>CADASTRAR NOVO SERVIÇO</h1>
      <form class="col-md-10" method="post" action="{{ route('insertServico') }}" enctype="multipart/form-data">
        <div class="form-group">
          <label for="exampleFormControlInput1">Titulo</label>
          <input type="text" class="form-control" id="exampleFormControlInput1" name="nome" required>
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Breve descrição do serviço</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="descricao" required></textarea>
        </div>
        <div class="form-group">
          <label for="exampleFormControlFile1">Foto exibida no slide show da pagina inicial</label>
          <input type="file" class="form-control-file" id="exampleFormControlFile1" name="foto_slideshow" required>
        </div>
        <div class="form-group">
          <label for="exampleFormControlFile1">Foto exibida na pagina serviços</label>
          <input type="file" class="form-control-file" id="exampleFormControlFile1" name="foto_principal" required>
        </div>
        <div class="form-group">
          <label for="exampleFormControlFile1">Fotos da galeria do servico</label>
          <input type="file" class="form-control-file" id="exampleFormControlFile1" accept="image/*" name="galeria[]" required multiple>
        </div>
        <input type="hidden" name="page_fk" value="2">
        @csrf
        <div class="form-group row justify-content-md-center">
          <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </form>

    </div>
  </div>

  <body>
    @endsection