@extends('templates/site')
@section('content')
<body>

<div class="container gallery-container">

    <h1>{{$servico->nome}}</h1>

    <p class="page-description">{{$servico->descricao}}</p>

    <hr/>
    <h2 class="page-description text-center">Galeria</h2>
    
    <div class="tz-gallery">

        <div class="row">
            @foreach($fotos as $foto)
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <a class="lightbox" href="{{url($foto->foto)}}">
                            <img src="{{url($foto->foto)}}" alt="img">
                        </a>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

</div>
		<!-- Call to Action -->
		<section id="mu-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-about-area">
                        <!-- Title -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mu-title">
                                    <h2>Entre em contato e solicite agora um orçamento gratuito</h2>
                                    <a class="mu-primary-btn" href="/contato">Entrar em contato<span class="fa fa-long-arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
</body>


@endsection