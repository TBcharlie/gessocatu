@extends('templates/painel')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Conteudo {{ $pagina->titulo }}</h1>
    @if($pagina->id == 1)
    <a href="{{ route("gestSobre") }}" class="btn btn-danger btn-icon-split">
      <span class="icon text-white-50">
        <i class="fas fa-chevron-left"></i>
      </span>
      <span class="text">Voltar</span>
    </a>
    @endif
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Titulo: {{ $conteudo->titulo}}</h6>
      </div>
      <div class="card-body">
        @if (session('messagem'))
        <div class="alert alert-success">
            {{ session('messagem') }}
        </div>
    @endif
  @if (session('falha'))
    <div class="alert alert-danger">
        {{ session('falha') }}
    </div>
  @endif
  @if($pagina->id == 1)
        <form method="post" action="{{ route('conteudoAlterar',$conteudo->id) }}" >
          @endif
          <form method="post" action="{{ route('conteudoAltContato',$conteudo->id) }}" >
        @csrf
        <div class="card shadow mb-4">
          <div class="form-row d-flex justify-content-center">
        <div class="col-xs-12 campo">
          <div class="input-group mb-6">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm">Titulo</span>
            </div>
            <input type="text" class="form-control" placeholder="titulo" name="titulo" aria-describedby="basic-addon1" value="{{ $conteudo->titulo }}">
          </div>
        </div>
          </div>
        <div class="form-row d-flex justify-content-center">
        <div class="col-xs-12 campo">
          <div class="input-group mb-6">
            <div class="input-group-prepend">
        <script src="https://cdn.ckeditor.com/ckeditor5/20.0.0/classic/ckeditor.js"></script>
        <textarea name="conteudo" id="editor" style="width: 150px;">{{ $conteudo->conteudo }}</textarea>
        <script>
          ClassicEditor
              .create( document.querySelector( '#editor' ) )
              .catch( error => {
                  console.error( error );
              } );
      </script>
            </div>
          </div>
          </div>
          </div>
        </div>
      <button type="submit" class="btn btn-primary btn-block"><span>Enviar</span></button>
        </form>
      </div>
    </div>

  </div>

@endsection