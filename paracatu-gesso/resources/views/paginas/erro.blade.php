@extends('templates/site')
@section('content')
<!-- Start Page Header area -->
<div id="mu-page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-page-header-area">
                    <h1 class="mu-page-header-title">{{$error}} Pagina</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header area -->
<!-- End Breadcrumb -->
<!-- Start main content -->
<main>
    <!-- Start 404 Error -->
    <section id="mu-error">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-error-area">
                        <!-- Title -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mu-title">
                                    <h2>{{ $msg }}</h2>
                                    <p>{{ $submsg }}</p>
                                </div>
                            </div>
                        </div>
                            <!-- Start Contact Content -->
                        <div class="mu-error-content">
                            <div class="mu-error-text">
                                <div class="mu-backdrop">
                                    <p class="mu-text">{{ $error }}</p>
                                    <div class="mu-overlay"></div>
                                </div>
                            </div>
                            <a href="#" class="mu-back-to-home">Voltar Para Inicio<span class="fa fa-long-arrow-right"></span></a>
                                
                        </div>
                        <!-- End 404 Error  Content -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End 404 Error  -->

</main>
@endsection