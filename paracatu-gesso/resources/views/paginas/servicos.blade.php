@extends('templates/site')
@section('content')



<!-- Start main content -->
<main>
	<!-- Start Services -->
	<section id="mu-service">
		<div class="col-md-12 container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-service-area">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
									<h2>Nossos Serviços</h2>
									<!--p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa cum sociis.</p!-->
								</div>
							</div>
						</div>
						<!-- Start Service Content -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-service-content">
									<div class="row">
										<!-- Start single service -->
										@foreach($servicos as $servico)
										<div class="col-sm-12 col-md-6 ">
											<div class="mu-single-service">
												<div class="row">
													<a class="col-md-6" href="{{url('servico/'.$servico->id)}}">
														<img src="{{$servico->foto_principal}}" alt="img">
													</a>

													<div class="col-md-6 mu-single-service-content">
														<a href="/servico/{{$servico->id}}">
															<h3> {{$servico->nome}}</h3>
														</a>
														<p> {{$servico->descricao}} </p>
													</div>
												</div>
											</div>
										</div>
										@endforeach
										<!-- End single service -->
									</div>
								</div>
							</div>
						</div>
						<!-- End Service Content -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Services -->

	<!-- Call to Action -->
	<section id="mu-about">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-about-area">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
									<h2>Entre em contato e solicite agora um orçamento gratuito</h2>
									<a class="mu-primary-btn" href="/contato">Entrar em contato<span class="fa fa-long-arrow-right"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	</div>

</main>

@endsection