@extends('templates/painel')
@section('content')

<div class="row justify-content-md-center">
  <div class="row">
    <h1>ALTERAR SERVIÇO ({{$servico->nome}})</h1>
  </div>
</div>

@if (session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
    @endif
    @if (session('error'))
    <div class="alert alert-danger">
      {{ session('error') }}
    </div>
    @endif
<div class="row">
  <div class="col-6">
    <div class="row justify-content-md-center service_form">
      <form class="col-md-10 justify-self-content-" method="post" action="{{ route('alterarServico', $servico->id) }}" enctype="multipart/form-data">
        <div class="form-group">
          <label for="exampleFormControlInput1">Alterar Titulo</label>
          <input type="text" class="form-control" id="exampleFormControlInput1" name="nome" value="{{$servico->nome}}" >
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Alterar descrição do serviço</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="descricao" >{{$servico->descricao}}</textarea>
        </div>
        <div class="form-group">
          <label for="exampleFormControlFile1">Alterar foto exibida no slide show da pagina inicial</label>
          <input type="file" class="form-control-file" id="exampleFormControlFile1" name="foto_slideshow" >
          <label for="exampleFormControlTextarea1">Foto atual</label>
          <img class="col-md-4" src="{{ url('/') }}{{$servico->foto_slideshow}}" alt="img">
          <img>
        </div>
        <div class="form-group">
          <label for="exampleFormControlFile1">Alterar Foto exibida na pagina serviços</label>
          <input type="file" class="form-control-file" id="exampleFormControlFile1" name="foto_principal" >
          <label for="exampleFormControlTextarea1">Foto atual</label>
          <img class="col-md-4" src="{{ url('/') }}{{$servico->foto_principal}}" alt="img">
        </div>
        <input type="hidden" name="page_fk" value="2">
        @csrf
        <div class="form-group row justify-content-md-center">
          <button type="submit" class="btn btn-primary">Alterar</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-6">
    <div class="table-responsive">
      <table class="table table-bordered" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Serviços</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Serviços</th>
            <th>Ações</th>
          </tr>
        </tfoot>
        <tbody>
          <label for="exampleFormControlInput1">Fotos da galeria</label>
          
          @foreach($fotos as $foto)
          <tr>
            <td>
              <div class="col-md-12 ">
                <div class="mu-single-service">
                  <div class="row">
                    <a class="col-md-4">
                      <img class="col-md-12" src="{{ url('/') }}{{$foto->foto}}" alt="img">
                    </a>
                  </div>
                </div>
              </div>
            </td>
            <td>
              <div class="row">
                <div class="col-md-12">
                  <a href="{{url('adm/foto_delet/'.$foto->id)}}">
                    <span class="material-icons" style="font-size: 48px">delete</span>
                  </a>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <form class="col-md-10" method="post" action="{{ route('addFoto') }}" enctype="multipart/form-data">
        @csrf
        
        <div class="form-group">
          <label for="exampleFormControlFile1">Selecionar fotos da galeria do servico</label>
          <input type="file" class="form-control-file" id="exampleFormControlFile1" accept="image/*" name="galeria[]" required multiple>
        </div>
        <div class="form-group row justify-content-md-center">
          <button type="submit" class="btn btn-primary">Adicionar fotos</button>
        </div>
        <input type="hidden" name="page_fk" value="2">
        <input type="hidden" name="servico_id" value="{{$servico->id}}">
        <input type="hidden" name="servico_nome" value="{{$servico->nome}}">
      </form>
    </div>
  </div>
</div>


@endsection