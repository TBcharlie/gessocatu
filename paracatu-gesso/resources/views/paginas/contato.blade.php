@extends('templates/site')
@section('content')
<script>
        function formatar(mascara, documento){
          var i = documento.value.length;
          var saida = mascara.substring(0,1);
          var texto = mascara.substring(i)
          
          if (texto.substring(0,1) != saida){
                    documento.value += texto.substring(0,1);
          }
          
        }

        </script>
<section id="mu-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-contact-area">
                    <!-- Title -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mu-title">
                                <h2>{{ $conteudo->titulo }}</h2>
                                <?=$conteudo->conteudo;?>
                            </div>
                        </div>
                    </div>
                        <!-- Start Contact Content -->
                    <div class="mu-contact-content">
                        <div class="row">
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script> 
                            <div class="col-md-12">
                                <div class="mu-contact-form-area">
                                    <div id="form-messages"></div>
                                    <form id="ajax-contact" method="post" action="{{ route('contatoCads') }}" class="mu-contact-form">
                                        @csrf
                                        <div class="form-group">  
                                            <span class="fa fa-user mu-contact-icon"></span>              
                                            <input type="text" class="form-control" placeholder="Nome" id="name" name="name" required>
                                        </div>
                                        <div class="form-group">  
                                            <span class="fa fa-phone-square mu-contact-icon"></span>            
                                            <input type="tel" class="form-control phone-mask" placeholder="Telefone" id="telefone" name="telefone" maxlength="12" OnKeyPress=" formatar('## ####-####', this)" required>
                                        </div>
                                        <div class="form-group">  
                                            <span class="fa fa-envelope mu-contact-icon"></span>              
                                            <input type="email" class="form-control" placeholder="Digite o e-mail" id="email" name="email" required>
                                        </div>    
                                        <div class="form-group">  
                                            <span class="fa fa-map mu-contact-icon"></span>           
                                            <input type="text" class="form-control" placeholder="Estado" id="estado" name="estado" required>
                                        </div>
                                        <div class="form-group">  
                                            <span class="fa fa-street-view mu-contact-icon"></span>              
                                            <input type="text" class="form-control" placeholder="Cidade" id="cidade" name="cidade" required>
                                        </div>
                                        <div class="form-group"> 
                                            <span class="fa fa-folder-open-o mu-contact-icon"></span>                
                                            <input type="text" class="form-control" placeholder="Assunto" id="subject" name="subject" required>
                                        </div>
            
                                        <div class="form-group">
                                            <span class="fa fa-pencil-square-o mu-contact-icon"></span> 
                                            <textarea class="form-control" placeholder="Mensagem..." id="message" name="message" required></textarea>
                                        </div>
                                        <button type="submit" class="mu-send-msg-btn"><span>Enviar</span></button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- End Contact Content -->
                </div>
            </div>
        </div>
    </div>
</section>

<div id="mu-google-map">
    <iframe src=" {{ $infoGeral->posMapa }} " width="850" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>


<script type="text/javascript">
$(document).ready(function(){
    $('#telefone').mask("(99) 99999-9999");
});
</script>
@endsection