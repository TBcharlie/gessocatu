@extends('templates/painel')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Messagem de contato</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Conteudos da pagina sobre</h6>
      </div>
      @if (session('messagem'))
      <div class="alert alert-success">
          {{ session('messagem') }}
      </div>
  @endif
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Ações</th>
                <th>Titulo</th>
                <th>Data Modificação</th>
              </tr>
            </thead>
            <tfoot>
                <th>Ações</th>
                <th>Titulo</th>
                <th>Data Modificação</th>
            </tfoot>
            <tbody>
              @foreach($lista as $cont)
              <tr>
                <td>
                  <a href="{{ route('conteudoMostrar',$cont->id) }}"><i class="fa fa-file"></i></a>
                  @if($cont->visivel == 1)
                  <a href="{{ route('conteudoOcultar',$cont->id) }}"><i class="fa fa-eye"></i></a>
                  @else
                  <a href="{{ route('conteudoOcultar',$cont->id) }}"><i class="fa fa-eye-slash"></i></a>
                  @endif
                </td>
                <td>{{$cont->titulo}}</td>
                <td>{{$cont->update_at}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>

@endsection