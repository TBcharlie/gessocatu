@extends('templates/painel')
@section('content')
<style>
   
     .campo{
       margin-bottom: 2%;
     }
   
</style>

<div class="container">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Informações basicas</h1>
    <div class="card shadow mb-4">
      @if (session('messagem'))
      <div class="alert alert-success">
          {{ session('messagem') }}
      </div>
  @endif
  @if (session('falha'))
  <div class="alert alert-danger">
      {{ session('falha') }}
  </div>
@endif
      <form method="post" action="{{ route('infoUp') }}" >
      @csrf
        <div class="form-row d-flex justify-content-center">
      <div class="col-xs-6 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Nome da empresa</span>
          </div>
          <input type="text" class="form-control" placeholder="Nome da empresa" name="nome" aria-describedby="basic-addon1" value="{{ $info->nome }}">
        </div>
      </div>

      <div class="col-xs-6 campo">
        <div class="input-group mb-6 ">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">CNPJ</span>
          </div>
          <input type="text" class="form-control" placeholder="CNPJ da empresa" name="cnpj" aria-describedby="basic-addon1" value="{{ $info->cnpj }}">
        </div>
      </div>
        </div>

        <div class="form-row d-flex justify-content-center">
          <div class="col-xs-6 col-5 campo">
            <div class="input-group mb-6">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">Descricao</span>
              </div>
              <input type="text" class="form-control" placeholder="Descricao da empresa" name="descricao" aria-describedby="basic-addon1" value="{{ $info->descricao }}">
            </div>
          </div>
        </div>

        <p class="text-center h3 mb-2 text-gray-800 ">Contatos</p>
        <div class="form-row d-flex justify-content-center">
      <div class="col-xs-6 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Telefone</span>
          </div>
          <input type="telefone" class="form-control" placeholder="Telefone da empresa" name="telefone" aria-describedby="basic-addon1" value="{{ $info->telefone }}">
        </div>
      </div>

      <div class="col-xs-6 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">whatsapp</span>
          </div>
          <input type="text" class="form-control" placeholder="whatsapp da empresa" name="whatsapp" aria-describedby="basic-addon1" value="{{ $info->whatsapp }}">
        </div>
      </div>
        </div>
      <div class="col-xs-6 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">instagram</span>
          </div>
          <input type="text" class="form-control" placeholder="url do instagram da empresa" name="instagram" aria-describedby="basic-addon1" value="{{ $info->instagram }}">
        </div>
      </div>

      <div class="col-xs-6 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">email</span>
          </div>
          <input type="email" class="form-control" placeholder="exemplo@email.com" name="email" aria-describedby="basic-addon1" value="{{ $info->email }}">
        </div>
      </div>

      <p class="text-center h3 mb-2 text-gray-800 ">Localização</p>

      <div class="col-xs-6 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">localização no google</span>
          </div>
          <input type="text" class="form-control" placeholder="url da localização no google" name="posMapa" aria-describedby="basic-addon1" value="{{ $info->posMapa }}">
        </div>
      </div>

      <div class="form-row d-flex justify-content-center">
      <div class="col-xs-6 col-5 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Logadouro</span>
          </div>
          <input type="text" class="form-control" placeholder="logadouro da empresa" name="logadouro" aria-describedby="basic-addon1" value="{{ $info->logadouro }}">
        </div>
      </div>
      
      <div class="col-xs-3 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Numero</span>
          </div>
          <input type="text" class="form-control" placeholder="Numero do imovel" name="numLocal" aria-describedby="basic-addon1" value="{{ $info->numLocal }}">
        </div>
      </div>

      <div class="col-xs-3 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Bairro</span>
          </div>
          <input type="text" class="form-control" placeholder="bairro da empresa" name="bairro" aria-describedby="basic-addon1" value="{{ $info->bairro }}">
        </div>
      </div>

    </div>


    <div class="form-row d-flex justify-content-center">
      <div class="col-xs-6 col-5 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Cidade</span>
          </div>
          <input type="text" class="form-control" placeholder="cidade" name="cidade" aria-describedby="basic-addon1" value="{{ $info->cidade }}">
        </div>
      </div>

      <div class="col-xs-6 col-5 campo">
        <div class="input-group mb-6">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">Cep</span>
          </div>
          <input type="text" class="form-control" placeholder="cep" name="cep" aria-describedby="basic-addon1" value="{{ $info->cep }}">
        </div>
      </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block"><span>Enviar</span></button>
      </form>
    </div>

  </div>

@endsection