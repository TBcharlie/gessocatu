<!DOCTYPE html>
<html lang="pt-br">
  <head> 
    @include('templates/elementosPainel/head')
  </head>
  <body>
    <body id="page-top">

      <!-- Page Wrapper -->
      <div id="wrapper">
        @include('templates/elementosPainel/sidebar')
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content" class="content">
              @include('templates/elementosPainel/nav')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
              </div>
              @yield('content')
            </div>
            @include('templates/elementosPainel/footer')
          </div>
        </div>
      </div>
      @include('templates/elementosPainel/scroll')
      @include('templates/elementosPainel/modal')
	<!-- JavaScript -->
	@include('templates/elementosPainel/scrip')
  </body>
</html>