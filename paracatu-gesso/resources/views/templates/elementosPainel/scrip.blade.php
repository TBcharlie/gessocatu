<script src="{{ url('/') }}/assets/painel/vendor/jquery/jquery.min.js"></script>
<script src="{{ url('/') }}/assets/painel/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="{{ url('/') }}/assets/painel/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="{{ url('/') }}/assets/painel/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="{{ url('/') }}/assets/painel/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{ url('/') }}/assets/painel/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="{{ url('/') }}/assets/painel/js/demo/datatables-demo.js"></script>