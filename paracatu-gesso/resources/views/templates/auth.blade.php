<!DOCTYPE html>
<html lang="pt-br">

<head>
@include('templates/elementosAuth/head')
</head>
<body class="bg-gradient-primary">
<div class="container">
    @yield('content')
</div>
@include('templates/elementosAuth/script')
</body>
</html>