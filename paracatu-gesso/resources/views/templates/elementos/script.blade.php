 <!-- jQuery first, then Popper.js, then Bootstrap JS -->
 <script src="{{ url('/') }}/assets/js/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
 <script src="{{ url('/') }}/assets/js/bootstrap.min.js"></script>
 <!-- Slick slider -->
 <script type="text/javascript" src="{{ url('/') }}/assets/js/slick.min.js"></script>
 <!-- Progress Bar -->
 <script src="{{ url('/') }}/assets/js/circle.js"></script>
 
 <!-- Filterable Gallery js -->
 <!--script type="text/javascript" src="assets/js/jquery.filterizr.min.js"></script-->
 <!-- Gallery Lightbox -->
 <script type="text/javascript" src="{{ url('/') }}/assets/js/jquery.magnific-popup.min.js"></script>
 <!-- Counter js -->
 <script type="text/javascript" src="{{ url('/') }}/assets/js/counter.js"></script>
 <!-- Ajax contact form  -->
 <script type="text/javascript" src="{{ url('/') }}/assets/js/app.js"></script>
 
 
 <!-- Custom js -->
 <script type="text/javascript" src="{{ url('/') }}/assets/js/custom.js"></script>

 <!-- About us Skills Circle progress  -->
 <script>
     // First circle
     new Circlebar({
     element : "#circle-1",
     type : "progress",
       maxValue:  "90"
     });
     
     // Second circle
     new Circlebar({
     element : "#circle-2",
     type : "progress",
       maxValue:  "84"
     });

     // Third circle
     new Circlebar({
     element : "#circle-3",
     type : "progress",
       maxValue:  "60"
     });

     // Fourth circle
     new Circlebar({
     element : "#circle-4",
     type : "progress",
       maxValue:  "74"
     });

 </script>
 