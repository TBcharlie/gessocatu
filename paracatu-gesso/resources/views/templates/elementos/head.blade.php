<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Paracatu Gesso 
  @if($paginaAtual != 'Inicio')
  - {{ $paginaAtual }}
  @endif
</title>
<!-- declaraçã de icnone superior -->
<link rel="shortcut icon" type="image/icon" href="{{url('assets/images/favicon.ico')}}"/>
<!-- declaração de fonte -->
<link href="{{url('assets/css/font-awesome.min.css')}}" rel="stylesheet">
 <!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
<!-- Slick slider -->
<link href="{{url('assets/css/slick.css')}}" rel="stylesheet">
<!-- Gallery Lightbox -->
<link href="{{url('assets/css/magnific-popup.css')}}" rel="stylesheet">
<!-- Skills Circle CSS  -->
<link rel="stylesheet" type="text/css" href="https://unpkg.com/circlebars@1.0.3/dist/circle.css">
<!-- serviço especifico header -->

  <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">

<!-- Main Style -->
<link href="{{url('assets/css/style.css')}}" rel="stylesheet">

<!-- Fonts -->

<!-- Google Fonts Raleway -->
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700" rel="stylesheet">
<!-- Google Fonts Open sans -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700,800" rel="stylesheet">


