<!--botão da logo -->
<a class="scrollToTop" href="#">
  <i class="fa fa-angle-up"></i>
</a>
<!-- Menu -->
<header id="mu-hero">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light mu-navbar">
            <!-- Text based logo -->
            <a class="navbar-brand mu-logo" href="/"><span>Paracatu Gesso</span></a>
            <!-- image based logo -->
               <!--a class="navbar-brand mu-logo" href="index.html"><img src="assets/images/logo.png" alt="logo"></a-->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto mu-navbar-nav">
              @if($paginaAtual == 'Inicio')
                <li class="nav-item active">
              @else
                <li class="nav-item">
              @endif
              <a href="/">INICIO</a></li>

              @if($paginaAtual == 'Sobre')
                <li class="nav-item active">
              @else
                <li class="nav-item">
              @endif
              <a href="/sobre">Sobre nós</a></li>

              @if($paginaAtual == 'Serviços')
                <li class="nav-item active">
              @else
              <li class="nav-item">
              @endif
              <a href="/servicos">Serviços</a></li>
              
              @if($paginaAtual == 'Contato')
                <li class="nav-item active">
              @else
                <li class="nav-item">
              @endif
              <a href="/contato">Entre em contato</a></li>
            </ul>
          </div>
        </nav>
    </div>
</header>