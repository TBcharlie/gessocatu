	<footer id="mu-footer">
		<div class="mu-footer-top">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="mu-single-footer">
							<h2>Paracatu Gesso</h2>
							<!--img class="mu-footer-logo" src="{{url('assets/images/logo.png')}}" alt="logo"-->
							<p>{{ $infoGeral->descricao}}</p>
							<div class="mu-social-media">
								<a class="mu-facebook" target="_blank" href="https://www.facebook.com/paracatuges"><i class="fa fa-facebook"></i></a>
								
								@isset($infoGeral)								
								<a class="mu-pinterest" target="_blank" href="{{$infoGeral->instagram}}"><i class="fa fa fa-instagram"></i></a>
								<a class="mu-twitter" target="_blank" href="https://wa.me/55<?=preg_replace("/[^0-9]/",'',$infoGeral->whatsapp)?>"><i class="fa fa-whatsapp"></i></a>
								@endisset
							</div>
						</div>
					</div>

					<div class="col-md-6">
					</div>
					
					<div class="col-md-3">
						<div class="mu-single-footer">
							<h3>Contatos</h3>
							<ul class="list-unstyled">
							  <li class="media">
							    <span class="fa fa-home"></span>
							    <div class="media-body">
									@isset($infoGeral)	
									<p>{{$infoGeral->logadouro}}, {{$infoGeral->numLocal}}, Bairro {{$infoGeral->bairro}}, 
										{{$infoGeral->cidade }} - CEP {{$infoGeral->cep}}</p>
									@endisset
							    </div>
							  </li>
							  <li class="media">
							    <span class="fa fa-phone"></span>
							    <div class="media-body">
									@isset($infoGeral)	
								  {{$infoGeral->telefone}}
								  @endisset
							    </div>
							  </li>
							  <li class="media">
							    <span class="fa fa-whatsapp"></span>
							    <div class="media-body">
									@isset($infoGeral)	
								  {{$infoGeral->whatsapp}}
								  @endisset
							    </div>
							  </li>
							  <li class="media">
							    <span class="fa fa-envelope"></span>
							    <div class="media-body">
									@isset($infoGeral)	
								 <p>{{$infoGeral->email}}</p>
								 @endisset
							    </div>
							  </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="mu-footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-footer-bottom-area">
							<p class="mu-copy-right">&copy; Copyright Paracatu Gesso {{ date('yy') }}. Todos diretos reservados.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>