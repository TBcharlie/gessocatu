<!--Imagem header -->
<div id="mu-page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-page-header-area">
                    @if($paginaAtual == 'Contato')
                        <h1 class="mu-page-header-title">Entre em Contato</h1>
                    @endif
                    @if($paginaAtual == 'Sobre')
                        <h1 class="mu-page-header-title">Sobre a empresa</h1>
                    @endif
                    @if($paginaAtual == 'Serviços')
                        <h1 class="mu-page-header-title">Serviços</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>