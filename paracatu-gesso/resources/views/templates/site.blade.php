<!DOCTYPE html>
<html lang="pt-br">
  <head> 
    @include('templates/elementos/head')
  </head>
  <body>
  	<!-- Cabeçalho-->
  @include('templates/elementos/header')

  @isset($imageMenu)
    @include('templates/elementos/headerImagem')
  @endisset

  @isset($breadcrumb)
    @include('templates/elementos/breadcrumb')
  @endisset


	<!--Conteudo da pagina -->
	<main>	
		@yield('content')
	</main>	
			
	<!-- rotape -->
	@include('templates/elementos/footer')

	<!-- JavaScript -->
	@include('templates/elementos/script')
  </body>
</html>