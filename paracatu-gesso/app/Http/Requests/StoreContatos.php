<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreContatos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'telefone' => 'required|string|max:15',
            'email' => 'required|string|max:100',
            'estado' => 'required|string|max:50',
            'cidade' => 'required|string|max:50',
            'subject' => 'required|string|max:50',
            'message' => 'required|string'
        ];
    }
}
