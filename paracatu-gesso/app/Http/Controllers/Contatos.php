<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contato;
use App\Informacao;
use App\ConteudoPag;
use App\Paginas;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\Validator;

use function GuzzleHttp\Promise\all;

class Contatos extends Controller
{

    private $info;
    public function __construct()
    {
        $this->info = Informacao::all()->first();
    }
    //exibe a pagina contato
    public function index()
    {
        $paginas = Paginas::firstWhere('titulo', 'Contato');
        $conteudos = ConteudoPag::where('idPag', $paginas->id)->where('visivel',1)->first();


        $data = ['paginaAtual' => 'Contato', 
        'imageMenu' => True,
         'breadcrumb' => 'contato', 
         'infoGeral' => $this->info,
        'conteudo' => $conteudos];
        return view('paginas/contato', $data);
    }
    public function create()
    {
        return redirect('Contatos@store');
    }
    public function store(Request $request)
    {
        $contato = new contato;
        $validator = Validator::make($request->all(), $contato->rules());

        $erros = $validator->errors();
        $exibermsg = '';
        $i = 0;
        foreach ($erros->all() as $message) {
            if ($i == 0) {
                $exibermsg = $message;
            } else {
                $exibermsg = $exibermsg . '| ' . $message;
            }
            $i = $i + 1;
        }
        if ($validator->fails()) {
            return $exibermsg;
        }


        //request são os dados que vem do formulario, como isso usamos o metodo get para pegar os campos
        //exemplo $request->get('nome do campo') 
        $contato->nome = $request->get('name');
        $contato->telefone = $request->get('telefone');
        $contato->email = $request->get('email');
        $contato->uf = $request->get('estado');
        $contato->cidade = $request->get('cidade');
        $contato->assunto = $request->get('subject');
        $contato->mensagem = $request->get('message');
        $contato->status = 1;

        //Verifica se inseriu com sucesso
        if ($contato->save()) {
            return 'Sua mensagem foi recebida! 😎';
        } else {
            // Redireciona de volta com uma mensagem de erro
            return redirect()
                ->back()
                ->with('error', 'Falha ao inserir');
        }
    }

    public function list()
    {

        $mensagens = contato::all()->where('status', 1);
        return view('paginas/contatoPainel', ['lista' => $mensagens]);
    }

    public function show($id)
    {

        $mensagem = contato::find($id);
        return view('paginas/contatoMostrarPainel', ['mensagem' => $mensagem]);
    }

    function update($id)
    {
        $mensagem = contato::findOrFail($id);
        $mensagem->status = 0;

        $mensagem->save();

        return redirect('adm/contato');
    }

    public function exibeConteudo(){
        $paginas = Paginas::firstWhere('titulo', 'Contato');
        $conteudo = ConteudoPag::where('idPag', $paginas->id)->first();
        $data = ['conteudo' => $conteudo,'pagina'=>$paginas];
        return view('paginas/conteudoMostrarPainel', $data);
    }
     public function edit(Request $request,$id)
    {
        $conteudo = ConteudoPag::findOrFail($id);
        $i = 0;
        if ($conteudo->titulo != $request->titulo) {
            $conteudo->titulo = $request->titulo;
            $i += 1;
        }
        if ($conteudo->conteudo != $request->conteudo) {
            $conteudo->conteudo = $request->conteudo;
            $i += 1;
        }

        if ($i == 0) {
            return redirect()->back()
                ->with('falha', 'Informações passadas não possui alteração. 😥');
        } else {
            if ($conteudo->save()) {
                return redirect()->back()
                    ->with('messagem', "Informações alteradas com sucesso 😎. Total $i campos alterados.");
            }else{
                return redirect()->back()
                ->with('falha', 'Falha em alterar as informações 😯');
            }
        }
    }
}
