<?php

namespace App\Http\Controllers;

use App\Informacao;
use App\ConteudoPag;
use App\Paginas;
use App\Servicos;
use Illuminate\Http\Request;

class PaginaPrincipal extends Controller
{
    public function index()
    {
        $servicos = Servicos::all();
        $paginas = Paginas::firstWhere('titulo', 'Sobre');
        $sobre = ConteudoPag::firstWhere('idPag', $paginas->id);
        $sobre = ConteudoPag::firstWhere('idPag', $paginas->id);
        $data = [
            'paginaAtual'=>'Inicio',
            'infoGeral' => Informacao::all()->first(),
            'sobre' => $sobre,
            'servicos' => $servicos,
        ];
        return view('paginas/home', $data);
    }
}
