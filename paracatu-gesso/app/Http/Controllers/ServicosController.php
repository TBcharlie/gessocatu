<?php

namespace App\Http\Controllers;

use App\Fotos;
use App\Servicos;
use App\Informacao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class ServicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        return view('paginas/servicos', [
            'imageMenu' => True,
            'breadcrumb' => 'servico',
            'paginaAtual' => 'Serviços',
            'infoGeral' => Informacao::all()->first(),
            'servicos' => Servicos::all()
        ]);
    }

    public function indexPainel()
    {
        $servicos = Servicos::all();
        $dados = [
            'servicos' => $servicos,
        ];
        return view('paginas/servico_painel', $dados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paginas/novo_servico_painel');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $servico = new Servicos;

        $servico->page_fk = $request->get('page_fk');
        $servico->descricao = $request->get('descricao');
        $servico->nome = $request->get('nome');


        if ($request->hasFile('foto_principal')) {
            $foto_principal_name = md5(uniqid("")) . "." . $request->file('foto_principal')->getClientOriginalExtension();
            $path_way_principal = '/assets/services_img/services_principal/' . $foto_principal_name;
            $servico->foto_principal = $path_way_principal;
            $request->file('foto_principal')->move(public_path() . '/assets/services_img/services_principal', $foto_principal_name);
        }
        if ($request->hasFile('foto_slideshow')) {
            $foto_slideshow_name = md5(uniqid("")) . "." . $request->file('foto_slideshow')->getClientOriginalExtension();
            $path_way_slideshow = '/assets/services_img/services_slideshow/' . $foto_slideshow_name;
            $servico->foto_slideshow = $path_way_slideshow;
            $request->file('foto_slideshow')->move(public_path() . '/assets/services_img/services_slideshow', $foto_slideshow_name);
        }

        $serviceResult = $servico->save();

        if ($request->hasFile('galeria')) {


            foreach ($request->file('galeria') as $img) {
                $fotos = new Fotos();

                $fotos->titulo = $request->get('nome');
                $fotos->descricao = 'foto da galeria de servico';
                $fotos->visivel = 1;
                $fotos->tipo = 'service_galery';
                $fotos->page_fk = 2;
                $fotos->servico_fk = $servico->id;

                $foto_galery_name = md5(uniqid("")) . "." . $img->getClientOriginalExtension();
                $path_way_foto_galery = '/assets/services_img/services_gallery/' . $foto_galery_name;
                $fotos->foto = $path_way_foto_galery;
                $img->move(public_path() . '/assets/services_img/services_gallery', $foto_galery_name);

                $fotosResult = $fotos->save();
            }
        }



        if ($serviceResult and $fotosResult) {
            return redirect()->back()->with('success', 'Serviço cadastrado com sucesso.');
        } else {
            return redirect()->back()->with('error', 'Erro ao cadastrar serviço');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fotos = Fotos::where('servico_fk', $id)->get();
        $servico = Servicos::where('id',$id)->first();

        return view('paginas/servico', [
            'fotos' => $fotos,
            'paginaAtual' => 'Serviço',
            'imageMenu' => True,
            'breadcrumb' => 'Servico',
            'paginaAtual' => 'Serviço',
            'infoGeral' => Informacao::all()->first(),
            'servico' => $servico
        ]);
        //return view('paginas/servico');
        print_r(array('hello world', $id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $servico = Servicos::find($id);
        $fotos = Fotos::where('servico_fk', $id)->get();

        $dados = [
            'id' => $id,
            'servico' => $servico,
            'fotos' => $fotos
        ];

        return view('/paginas/edit_servico_painel', $dados);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $servico = Servicos::find($id);
        $alter = 0;

        if($servico->nome != $request->get('nome')){
            $servico->nome = $request->get('nome');
            $alter++;
        }
        if($servico->descricao != $request->get('descricao')){
            $servico->descricao = $request->get('descricao');
            $alter++;
        }
        
        if ($request->hasFile('foto_principal')) {
            $fotoPrincipal = public_path($servico->foto_principal);
            if (File::exists($fotoPrincipal)) {
                File::delete($fotoPrincipal);
            }
            $foto_principal_name = md5(uniqid("")) . "." . $request->file('foto_principal')->getClientOriginalExtension();
            $path_way_principal = '/assets/services_img/services_principal/' . $foto_principal_name;
            $servico->foto_principal = $path_way_principal;
            $request->file('foto_principal')->move(public_path() . '/assets/services_img/services_principal', $foto_principal_name);
            $alter++;
        }
        if ($request->hasFile('foto_slideshow')) {
            $fotoSlideshow = public_path($servico->foto_slideshow);
            if (File::exists($fotoSlideshow)) {
                File::delete($fotoSlideshow);
            }
            $foto_slideshow_name = md5(uniqid("")) . "." . $request->file('foto_slideshow')->getClientOriginalExtension();
            $path_way_slideshow = '/assets/services_img/services_slideshow/' . $foto_slideshow_name;
            $servico->foto_slideshow = $path_way_slideshow;
            $request->file('foto_slideshow')->move(public_path() . '/assets/services_img/services_slideshow', $foto_slideshow_name);
            $alter++;
        }
        if($alter != 0){
            $servico->save();
            return redirect()->back()->with('success', "Serviço alterado com sucesso! $alter alterações efetuadas.");
        }else{
            return redirect()->back()->with('error', 'Nenhuma alteração efetuada.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fotos = Fotos::where('servico_fk', $id)->get();
        $servico = Servicos::where('id', $id)->get();

        $fotoprincipal = public_path($servico[0]->foto_principal);
        $foto_slideshow = public_path($servico[0]->foto_slideshow);

        foreach ($fotos as $foto) {
            $fotoGalery = public_path($foto->foto);
            if (File::exists($fotoGalery)) {
                File::delete($fotoGalery);
            } else {
                return redirect()->back()->with('error', 'Imagem não existente no servidor.');
            }
        }

        if (File::exists($fotoprincipal) and File::exists($foto_slideshow)) {

            File::delete($fotoprincipal);
            File::delete($foto_slideshow);
        } else {
            return redirect()->back()->with('error', 'Imagem não existente no servidor.');
        }

        $resultFotos = Fotos::where('servico_fk', $id)->delete();
        $resultServico = Servicos::where('id', $id)->delete();

        if ($resultServico and $resultFotos) {
            return redirect()->back()->with('success', 'Serviço deletado com sucesso!');
        } else {
            return redirect()->back()->with('error', 'Erro ao deletar serviço.');
        }
    }


    public function deletFoto($id)
    {
        $foto = Fotos::findOrFail($id);

        $caminhoFoto = public_path($foto->foto);
        if (File::exists($caminhoFoto)) {
            File::delete($caminhoFoto);
        } else {
            return redirect()->back()->with('error', 'Imagem não existente no servidor.');
        }
        if (Fotos::find($id)->delete()) {
            return redirect()->back()->with('success', 'Foto deletada com sucesso.');
        } else {
            return redirect()->back()->with('error', 'Erro, ao deletar imagem.');
        }
    }

    public function addFoto(Request $request)
    {
        if ($request->hasFile('galeria')) {

            foreach ($request->file('galeria') as $img) {
                $fotos = new Fotos();

                $fotos->titulo = $request->get('servico_nome');;
                $fotos->descricao = 'foto da galeria de servico';
                $fotos->visivel = 1;
                $fotos->tipo = 'service_galery';
                $fotos->page_fk = 2;
                $fotos->servico_fk = $request->get('servico_id');

                $foto_galery_name = md5(uniqid("")) . "." . $img->getClientOriginalExtension();
                $path_way_foto_galery = '/assets/services_img/services_gallery/' . $foto_galery_name;
                $fotos->foto = $path_way_foto_galery;
                $img->move(public_path() . '/assets/services_img/services_gallery', $foto_galery_name);

                $fotosResult = $fotos->save();
                if (!$fotosResult) {
                    return redirect()->back()->with('error', 'Erro ao adicionar imagem.');
                }
            }
            return redirect()->back()->with('success', 'Imagens adicionada com sucesso.');
        }
    }
}
