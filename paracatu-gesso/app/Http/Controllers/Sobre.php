<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Informacao;
use App\ConteudoPag;
use App\Paginas;


class Sobre extends Controller
{
    public function index()
    {
        $paginas = Paginas::firstWhere('titulo', 'Sobre');
        $conteudos = ConteudoPag::where('idPag', $paginas->id)->where('visivel',1)->get();
        $data = [
            'imageMenu' => True,
            'breadcrumb' => 'Sobre',
            'paginaAtual' => 'Sobre',
            'infoGeral' => Informacao::all()->first(),
            'conteudo' => $conteudos
        ];
        return view('paginas/sobrenos', $data);
    }


    public function list()
    {
        $paginas = Paginas::firstWhere('titulo', 'Sobre');
        $conteudos = ConteudoPag::all()->where('idPag', $paginas->id);
        return view('paginas/conteudoPainel', ['lista' => $conteudos]);
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paginas = Paginas::firstWhere('titulo', 'Sobre');
        $conteudo = ConteudoPag::findOrFail($id);


        $data = ['conteudo' => $conteudo,'pagina'=>$paginas];
        return view('paginas/conteudoMostrarPainel', $data);
    }


    public function edit(Request $request, $id)
    {
        $conteudo = ConteudoPag::findOrFail($id);
        $i = 0;
        if ($conteudo->titulo != $request->titulo) {
            $conteudo->titulo = $request->titulo;
            $i += 1;
        }
        if ($conteudo->conteudo != $request->conteudo) {
            $conteudo->conteudo = $request->conteudo;
            $i += 1;
        }

        if ($i == 0) {
            return redirect()->back()
                ->with('falha', 'Informações passadas não possui alteração. 😥');
        } else {
            if ($conteudo->save()) {
                return redirect()->action('sobre@list')
                    ->with('messagem', "Informações alteradas com sucesso 😎. Total $i campos alterados.");
            }else{
                return redirect()->back()
                ->with('falha', 'Falha em alterar as informações 😯');
            }
        }
    }

    public function oculta($id)
    {
        $conteudo = ConteudoPag::findOrFail($id);
        if ($conteudo->visivel == 1) {
            $conteudo->visivel = 0;
        } else {
            $conteudo->visivel = 1;
        }


        $conteudo->save();
        return redirect('adm/sobre');
    }
    public function update(Request $request, $id)
    {
        //
    }
}
