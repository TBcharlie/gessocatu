<?php

namespace App\Http\Controllers;

use App\Informacao;
use Illuminate\Http\Request;

class InformacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = Informacao::all()->first();
        $data = ['info' => $info];


        return view('paginas\infoPainel', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $info = Informacao::all()->first();
        $numCampos = 0;

        if ($request->get('nome') != $info->nome) {
            $info->nome = $request->get('nome');
            $numCampos += 1;
        }

        if ($request->get('cnpj') != $info->cnpj) {
            $info->cnpj = $request->get('cnpj');
            $numCampos += 1;
        }

        if ($request->get('descricao') != $info->descricao) {
            $info->descricao = $request->get('descricao');
            $numCampos += 1;
        }

        if ($request->get('telefone') != $info->telefone) {
            $info->telefone = $request->get('telefone');
            $numCampos += 1;
        }

        if ($request->get('whatsapp') != $info->whatsapp) {
            $info->whatsapp = $request->get('whatsapp');
            $numCampos += 1;
        }

        if ($request->get('instagram') != $info->instagram) {
            $info->instagram = $request->get('instagram');
            $numCampos += 1;
        }

        if ($request->get('email') != $info->email) {
            $info->email = $request->get('email');
            $numCampos += 1;
        }

        if ($request->get('posMapa') != $info->posMapa) {
            $info->posMapa = $request->get('posMapa');
            $numCampos += 1;
        }

        if ($request->get('logadouro') != $info->logadouro) {
            $info->logadouro = $request->get('logadouro');
            $numCampos += 1;
        }

        if ($request->get('bairro') != $info->bairro) {
            $info->bairro = $request->get('bairro');
            $numCampos += 1;
        }

        if ($request->get('numLocal') != $info->numLocal) {
            $info->numLocal = $request->get('numLocal');
            $numCampos += 1;
        }

        if ($request->get('cidade') != $info->cidade) {
            $info->cidade = $request->get('cidade');
            $numCampos += 1;
        }

        if ($request->get('cep') != $info->cep) {
            $info->cep = $request->get('cep');
            $numCampos += 1;
            echo "<pre>";
            echo $info->cep;
            echo "<pre>";
        }

        if ($numCampos == 0) {
            return redirect()->action('InformacaoController@index')
                ->with('falha', 'Informações passadas não possui alteração. 😥');
        } else {
            if ($info->save()) {
                return redirect()->action('InformacaoController@index')
                    ->with('messagem', "Informações alteradas com sucesso 😎. Total $numCampos campos alterados.");
            }else{
                return redirect()->action('InformacaoController@index')
                ->with('falha', 'Falha em alterar as informações 😯');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
