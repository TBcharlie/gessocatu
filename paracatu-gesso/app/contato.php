<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contato extends Model
{
    protected $fillable = ['nome','telefone','email','uf','cidade','assunto','mensagem','status
    '];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'contato';

    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'telefone' => 'required|string|max:15',
            'email' => 'required|string|max:100',
            'estado' => 'required|string|max:50',
            'cidade' => 'required|string|max:50',
            'subject' => 'required|string|max:50',
            'message' => 'required|string'
        ];
    }
}
