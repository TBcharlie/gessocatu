<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informacao extends Model
{
    protected $fillable = ['nome','telefone','descricao','email','uf','cidade','assunto','mensagem','status
    '];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'informacao';
}
