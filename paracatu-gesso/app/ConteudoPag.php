<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConteudoPag extends Model
{
    protected $fillable = ['titulo','conteudo','visivel','idPag'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'conteudo_pags';
}
