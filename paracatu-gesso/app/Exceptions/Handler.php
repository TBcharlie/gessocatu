<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        $msg = "";
        if ($this->isHttpException($exception)) {
            $error = $exception->getStatusCode();
            switch ($error) {

                case 403:
                    $data = ["error"=>$error,"msg"=>$msg,'paginaAtual'=>'Error 403'];
                    return Response::view('paginas/erro',['error'=>$error,'msg'=>$msg,'paginaAtual'=>'error 403'], 403);
        
                case 404:
                    $msg = "Pagina não encontrada";
                    $submsg = "Talvez o endereço informado esteja incompleto, mas se estiver tudo certo, a página que você procura pode ter sido removida ou não estar mais disponível. :(";
                    $data = ["error"=>$error,"msg"=>$msg,"submsg"=>$submsg,'paginaAtual'=>'Error 404'];
                    return Response::view('paginas/erro',$data, 404);
        
                case 500:
                    $data = ["error"=>$error,"msg"=>$msg,'paginaAtual'=>'Error405'];
                    return Response::view('paginas/erro', ['error'=>$error,'msg'=>$msg,], 500);
                                       
                    default:
                    return $this->renderHttpException($exception);
                    break;
            }
        } else {
            return parent::render($request, $exception);
        }
    }
}
