<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fotos extends Model
{
    protected $fillable = ['titulo','descricao','visivel','foto', 'tipo','page_fk','servico_fk'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'fotos';

    public function rules()
    {
        return [
            'titulo' => 'required|string|max:255',
            'descricao' => 'required|string|max:255',
            'visivel' => 'required|string|max:1',
            'foto' => 'required|string|max:255',
            'page_fk' => 'required|string|max:255',
            'servico_fk' => 'required|string|max:255',
        ];
    }
}
