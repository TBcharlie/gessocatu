<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Servicos extends Model
{
    protected $fillable = ['descricao', 'nome', 'page_fk', 'foto_slideshow', 'foto_principal'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'servicos';

    public function rules()
    {
        return [
            'nome' => 'required|string|max:100',
            'descricao' => 'required|string',
            'foto_slideshow' => 'required|string|max:50',
            'foto_principal' => 'required|string|max:50',
            'page_fk' => 'required|string|max:50',
        ];
    }

}
