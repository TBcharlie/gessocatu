<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paginas extends Model
{
    protected $fillable = ['titulo','descricao','visivel'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'paginas';
}
